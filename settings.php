<?php

$production = false;

return [

    "paths" => [

        "root"          => __DIR__,

        // this should be the document root as well
        "app"           => __DIR__ . "/app";

        // these are relative to app and should not be absolute paths
        "routes"        => "routes.php",
        "templates"     => "templates",
        "assets"        => "assets",

        "cache" => [
            "assets"    => 'cache',
            "templates" => "cache/templates"
        ]
    ],

    "assets" => [
        "cache"         => $production,
        "compress"      => $production,
    ],
];
