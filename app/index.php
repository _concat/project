<?php

// redirect all traffic here via Apache, Nginx etc.

// this is the project root, ie. parent of 'app'
$project_root = dirname(__DIR__);

// include the composer autoloader
$autoload = require("$project_root/vendor/autoload.php");

// include the settings file
$settings = require("$project_root/settings.php");

// create a new Concat application using the required settings
$app = new \Concat\Framework\Application($settings);

// run the application
$app->run();
