<?php

return [

    // note that leading and trailing slashes are optional,
    // so "/about" == "about/" == "/about/"

    // url syntax is PATH, DESTINATION, (NAME, [SUBROUTES])
    // name can be skipped when providing subroutes
    //

    // root path
    url("/")->to(function(){
        return "Hello World!";
    });
];
